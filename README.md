# :zap: **soal-shift-sisop-modul-3-ITA10-2022** :zap:

| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Firdho Kustiawan | 5027201005 |
| Sharira Saniane           | 5027201016 |
| Jovan Surya Bako          | 5027201013 | 
<br/>


## :large_blue_circle: **Soal 1** :large_blue_circle: 
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.<br>
<ol type="a">
    <li>Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.</li>
    <li>Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.</li>
    <li>Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.</li>
    <li>Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)</li>
    <li>Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.</li>
</ol>

Catatan:
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

Pertama-tama membuat variabel child id yang akan digunakan secara global kemudian mendeklarasikan array yang menampung thread dan mendeklarasikan beberapa variabel yang akan digunakan untuk execv
```c
#define NUM_DIR 2
#define NUM_GLOBAL 2
#define SIZE 100

pid_t child_id;

pthread_t tid[NUM_DIR];
pthread_t tidGlobal[NUM_GLOBAL]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread

char __create[] = "/bin/mkdir";
char __createT[] = "/usr/bin/touch";
char __download[] = "/bin/wget";
char __unzip[] = "/usr/bin/unzip";
char __decode[] = "/bin/base64";
char __move[] = "/usr/bin/mv";
char __zip[] = "/usr/bin/zip";
char __delete[] = "/usr/bin/rm";
```
Setelah itu membuat fungsi `process` yang akan digunakan untuk memanggil execv secara berulang-ulang
```c
void process(char *str, char *argv[])
{
    pid_t child_id;
    int status;

    child_id = fork();

    if(child_id == 0){
		execv(str, argv);
	}
    else
    {
        while (wait(&status) > 0)
        ;
    }
}
```
Kemudian membuat fungsi `checkError` 
```c
void checkError(int err, int i)
{
    if(err!=0) //cek error
    {
        printf("\n can't create thread : [%s]", strerror(err));
    }
    else
    {
        printf("\n create thread [%d] success\n", i + 1);
    }
}
```

## **1a**
Membuat fungsi `createFolder` untuk membuat folder music dan quote sekaligus file `music.txt` dan `quote.txt`di folder yang sesuai </br>
- Fungsi main
    ```c
    /*
    |   CREATE FOLDER
    */
    for(int i = 0; i<NUM_DIR; i++) // loop sejumlah thread
    {
        err=pthread_create(&(tid[i]), NULL, &createFolder, NULL); //membuat thread
        checkError(err, i);
    }
    pthread_join(tid[0],NULL);
    pthread_join(tid[1],NULL);
    ```
- Fungsi `createFolder`
    ```c
    void* createFolder(void *arg)
    {
        char *directory[NUM_DIR] = {
            "/home/firdho/modul3/quote/",
            "/home/firdho/modul3/music/"
        };

        char *directoryT[NUM_DIR] = {
            "/home/firdho/modul3/quote/quote.txt",
            "/home/firdho/modul3/music/music.txt"
        };

        pthread_t id = pthread_self();

        for (int i = 0; i<NUM_DIR; i++)
        {
            char dir[100], tmp[100];
            strcpy(dir, directory[i]);
            strcpy(tmp, directoryT[i]);

            if(pthread_equal(id, tid[i])) //thread untuk clear layar
            {
                char *argv1[] = {"mkdir", dir, "-p", NULL};
                char *argv2[] = {"touch", tmp, NULL};
                process(__create, argv1);
                process(__createT, argv2);
            }
        }
        return NULL;
    }
    ```
![output 1a.1](/img/1.1.png)
![output 1a.2](/img/1.2.png)
![output 1a.3](/img/1.3.png)
![output 1a.4](/img/1.4.png)
Setelah itu membuat fungsi `unZIP` untuk mengekstrak file `music.zip` dan `quote.zip` yang telah didownload sebelumnya
- Fungsi main
    ```c
    /*
    |   UNZIP
    */
    for(int i = 0; i<NUM_GLOBAL; i++) // loop sejumlah thread
    {
        err=pthread_create(&(tidGlobal[i]), NULL, &unZIP, NULL); //membuat thread
        checkError(err, i);
    }
    pthread_join(tidGlobal[0],NULL);
    pthread_join(tidGlobal[1],NULL);
    ```
- Fungsi `unZIP`
    ```c
    void* unZIP(void *arg)
    {
        char *directoryUNZIP[NUM_GLOBAL] = {
            "/home/firdho/modul3/quote",
            "/home/firdho/modul3/music"
        };

        char *folder[NUM_GLOBAL] = {
            "/home/firdho/Downloads/quote.zip",
            "/home/firdho/Downloads/music.zip"
        };

        pthread_t id = pthread_self();

        for (int i = 0; i<NUM_GLOBAL; i++)
        {
            char dir[100];
            char fold[100];
            strcpy(dir, directoryUNZIP[i]);
            strcpy(fold, folder[i]);

            
            if(pthread_equal(id, tidGlobal[i])) //thread untuk clear layar
            {
                //
                char *argv1[] = {"unzip", "-q", fold, "-d", dir, NULL};
                process(__unzip, argv1);
            }
            
        }
        return NULL;
    }
    ```
![output 1a.5](/img/1.5.png)
![output 1a.6](/img/1.6.png)
## **1b**
Selanjutnya membuat fungsi `decode` untuk mendecode setiap isi file kemudian menuliskannya di file.txt yang sesuai
- Fungsi main
    ```c
    /*
    |   DECODE 
    */
    for(int i = 0; i<NUM_GLOBAL-1; i++) // loop sejumlah thread
    {
        err=pthread_create(&(tidGlobal[i]), NULL, &decodeIsi, NULL); //membuat thread
        checkError(err, i);
    }
    pthread_join(tidGlobal[0],NULL);
    pthread_join(tidGlobal[1],NULL);
    ```
- Fungsi `decode`
    ```c
    void* decodeIsi(void *arg)
    {
        char *directoryDecoded[NUM_GLOBAL] = {
            "/home/firdho/modul3/quote/",
            "/home/firdho/modul3/music/"
        };    

        char *destination[NUM_GLOBAL] = {
            "/home/firdho/modul3/quote/quote.txt",
            "/home/firdho/modul3/music/music.txt"
        }; 

        char *adder[NUM_GLOBAL] = {
            "q",
            "m"
        };

        pthread_t id = pthread_self();

        for (int i = 0; i<NUM_GLOBAL; i++)
        {
            char dir[100];
            char add[100];
            char temp[100];
            char str[100], value[100];
            
            if(pthread_equal(id, tidGlobal[i])){
                for(int j = 1; j<10; j++){
                    strcpy(dir, directoryDecoded[i]);
                    strcpy(add, adder[i]);
                    strcat(dir, add);
                    sprintf(temp, "%d.txt", j);
                    strcat(dir, temp);

                    printf("hasil dir: %s \n", dir);

                    
                    FILE *file = fopen(dir, "r"), *dest;
                    if(file == NULL){
                        fclose(file);
                        printf("Error! opening file");
                    }
                    else{
                        printf("Yeayy ke buka \n");
                    }

                    
                    while (fgets(str, 100, file) != NULL) {
                        printf("%s \n", str);
                        
                        //untuk decode
                        unsigned char *value = base64_decode(str);
                        printf("%s \n", value);

                        //untuk nulis hasil decode
                        dest = fopen(destination[i], "a");
                        fprintf(dest, "%s\n", value);
                    }
                    fclose(dest);
                    fclose(file);
                    
                    memset(dir, 0, sizeof(dir));
                    memset(value, 0, sizeof(value));
                }
            }
        }
    }
    ```
- Fungsi base64
    ```c
    #include"base64.h"

    char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                        'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    char* base64_decode(char* cipher) {

        char counts = 0;
        char buffer[4];
        char* plain = malloc(strlen(cipher) * 3 / 4);
        int i = 0, p = 0;

        for(i = 0; cipher[i] != '\0'; i++) {
            char k;
            for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
            buffer[counts++] = k;
            if(counts == 4) {
                plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
                if(buffer[2] != 64)
                    plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
                if(buffer[3] != 64)
                    plain[p++] = (buffer[2] << 6) + buffer[3];
                counts = 0;
            }
        }

        plain[p] = '\0';    /* string padding character */
        return plain;
    }
    ```
![output 1b.7](/img/1.7.png)
![output 1b.8](/img/1.8.png)
## **1c**
Membuat fungsi `moveFile` untuk mengecek apa folder hasil sudah ada atau belum jika belum maka dibuat kemudian file `music.txt` dan `quote.txt`dipindahkan ke folder hasil tersebut
- Fungsi main
    ```c
    /*
    |   MOVE
    */
    for(int i = 0; i<NUM_GLOBAL; i++) // loop sejumlah thread
    {
        err=pthread_create(&(tidGlobal[i]), NULL, &moveFile, NULL); //membuat thread
        checkError(err, i);
    }
    pthread_join(tidGlobal[0],NULL);
    pthread_join(tidGlobal[1],NULL);
    ```
- Fungsi `moveFile` 
    ```c
    void* moveFile(void *arg)
    {
        char *directoryMove[NUM_GLOBAL] = {
            "/home/firdho/modul3/quote/quote.txt",
            "/home/firdho/modul3/music/music.txt"
        };

        char folder[] = "/home/firdho/modul3/hasil";

        FILE *file;
        if(file = fopen(folder, "r")){
            fclose(file);
            printf("Folder sudah ada \n");
        }
        else{
            printf("Folder belum ada \n");

            char *argv1[] = {"mkdir", folder, "-p", NULL};
            process(__create, argv1);
        }

        sleep(2);

        pthread_t id = pthread_self();

        for (int i = 0; i<NUM_GLOBAL; i++)
        {
            char dir[100];
            strcpy(dir, directoryMove[i]);
            printf("%s \n", dir);
            
            if(pthread_equal(id, tidGlobal[i])) //thread untuk clear layar
            {
                //
                char *argv1[] = {"mv", dir, folder, NULL};
                process(__move, argv1);
            }

            memset(dir, 0, sizeof(dir));
        }
    }
    ```
![output 1c.9](/img/1.9.png)
![output 1c.10](/img/1.10.png)
## **1d**
Membuat fungsi `ziip` untuk mengzip file `music.txt` dan `quote.txt` dengan password tetapi dikarenakan pada tools dari library zip tidak bisa digunakan agar memasukkan password di codingan maka password akan dimasukkan secara manual melalui terminal kemudian dipindahkan 1 folder lebih depan
- Fungsi main
    ```c
    /*
    |   ZIP
    */
    chdir("/home/firdho/modul3/hasil");
    for(int i = 0; i<NUM_GLOBAL-1; i++) // loop sejumlah thread
    {
    err=pthread_create(&(tidGlobal[i]), NULL, &ziip, NULL); //membuat thread
        checkError(err, i);
    }
    pthread_join(tidGlobal[0],NULL);
    pthread_join(tidGlobal[1],NULL);
    ```
- Fungsi `ziip`
    ```c
    void* ziip(void *arg)
    {
        char *argv1[] = {"zip", "--encrypt", "hasil.zip", "music.txt", "quote.txt", NULL};
        process(__zip, argv1);
        sleep(2);
        char *argv2[] = {"mv", "hasil.zip", "/home/firdho/modul3", NULL};
        process(__move, argv2);
    }
    ```
![output 1d.11](/img/1.11.png)
![output 1d.12](/img/1.12.png)
## **1e**
Selanjutnya membuat fungsi `gass` yaitu untuk mengunzip folder `hasil.zip` kemudian mengecek apa file `no.txt` sudah ada atau belum jika belum maka akan dibuat dan akan dibuka file tersebut untuk dituliskan No. Setelah itu zip kembali file `music.txt`, `quote.txt`, dan `no.txt` dan hapus folder hasil serta file `music.txt`, `quote.txt`, dan `no.txt`
- Fungsi main
    ```c
    /*
    |   GASSSS
    */
    chdir("/home/firdho/modul3");
    for(int i = 0; i<NUM_GLOBAL-1; i++) // loop sejumlah thread
    {
        err=pthread_create(&(tidGlobal[i]), NULL, &gass, NULL); //membuat thread
        checkError(err, i);
    }
    pthread_join(tidGlobal[0],NULL);
    pthread_join(tidGlobal[1],NULL);
    ```
- Fungsi `gass`
    ```c
    void* gass(void *arg)
    {
        char *argv1[] = {"unzip", "-q", "hasil.zip", NULL};
        process(__unzip, argv1);

        //cek ada atau tidak file no.txt
        char folder[] = "no.txt";
        FILE *file, *dest;
        if(file = fopen(folder, "r")){
            fclose(file);
            printf("File sudah ada \n");
        }
        else{
            printf("File belum ada \n");

            char *argv1[] = {"touch", folder, NULL};
            process(__createT, argv1);
        }

        sleep(2);

        //buka dan tulis
        dest = fopen(folder, "a");
        fprintf(dest, "No");
        fclose(dest);
        
        sleep(2);
        char *argv3[] = {"zip", "--encrypt", "hasil.zip", "music.txt", "quote.txt", "no.txt",NULL};
        process(__zip, argv3);

        char *argv4[] = {"rm", "-r", "hasil", "music.txt", "quote.txt", "no.txt",NULL};
        process(__delete, argv4);

    }
    ```
![output 1e.13](/img/1.13.png)
![output 1e.14](/img/1.14.png)

## :large_blue_circle: **Soal 2** :large_blue_circle: 
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:
<ol type="a">
    <li>Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

    - Username unique (tidak boleh ada user yang memiliki username yang sama)
    - Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
</li>
    <li>Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.
</li>
    <li>Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
    
    - Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
    - Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
    - Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
    - Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.
</li>
    <li>Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). </li>
    <li>Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.
</li>
<li>Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”
</li>
<li>Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.
</li>
</ol>
Catatan:
 
 - Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.

## 2 a. Register dan Login
Client akan mencoba untuk melakukan koneksi dengan server menggunakan socket.Terdapat beberapa kasus yang akan menyebabkan kegagalan dalam pengubungan server dan client yang akan mengembalikan pesan error seperti kegagalan dalam pembuatan socket yang akan mengembalikan "Failed to create socket", kegagalan dalam pencarian alamat address yang akan mengembalikan "Invalid address", dan kegagalan penyambungan koneksi yang akan mengembalikan "Connection failed". Jika koneksi berhasil dibuat, akan muncul pesan agar client mengetikkan sesuatu supaya server bisa mengkonfirmasi bahwa koneksi telah terjalin. 

```
struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf(Failed to create socket);
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;

```
Setelah koneksi terjalin, selanjutnya muncul pilihan 1.Login, 2.Register, 3.Exit. Fungsi scanf akan mengambil command dari client dan menjalankan fungsi selanjutnya sesuai dengan command yang diberikan oleh client.

```
 while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
```

Jika client memasukkan command login, maka akan muncul pesan agar client memasukkan username dan password yang akan dikirimkan kepada server. Jika dikembalikan "LoginSuccess" dari server. 

```
while (1)
  {
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");
```
Server akan membaca informasi login yang dikirimkan oleh klien dan membuka file user.txt dengan fopen mode "r" untuk membaca data user yang tersedia. Server akan membaca file pada user.txt dan melakukan compare terhadap informasi login yang telah diberikan oleh user untuk menentukan apakah user berhasil login atau tidak.

```
 while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
        if (check == 0)
        printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
      else
      {
        printf("Authorization for Login is Success\n");
```
Jika register, akan muncul pesan untuk memasukkan informasi registrasi yang terdiri dari username dan password sesuai dengan ketentuan.

```
else if (same(cmd, regist))
    {
      send(sock, regist, strlen(regist), 0);
      memset(buffer, 0, sizeof(buffer));
      printf("Register\nUsername: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "RegError"))
        printf("Register gagal, username sudah terambil atau password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.\n");
      if (same(buffer, "Register berhasil"))
        printf("Register berhasil\n");
      memset(buffer, 0, sizeof(buffer));
    }
```
Di sisi server, server akan membuka file user.txt dengan mode "a" dan "r" untuk melakukan append dan membaca file user.txt. Server akan menerima informasi registrasi kemudian mengecek dengan melakukan compare antara username yang ada di file.

```
else if (same(regist, buffer))
    {
      fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
      readerConn = read(serverSocket, user.name, 1100),
      readerConn = read(serverSocket, user.password, 1100);
      int flag = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      int check = 0, numUsers = 0;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        numUsers++;
        char userName[105], userPass[105];
        int i = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i] = '\0';
        i++;
        int j = 0;
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0)
        {
          check = 0;
          break;
        }
        int kapital = 0, lower = 0, angka = 0, jumlah = 0;
        for (int i = 0; i < strlen(user.password); i++)
        {
          if (user.password[i] >= 48 && user.password[i] <= 57)
            angka = 1;
          if (user.password[i] >= 65 && user.password[i] <= 90)
            kapital = 1, jumlah++;
          if (user.password[i] >= 97 && user.password[i] <= 122)
            lower = 1, jumlah++;
        }
        if (!angka || !lower || !kapital || jumlah < 6)
          break;
        if (angka && lower && kapital && jumlah >= 6)
          check = 1;
      }
      if (check || numUsers == 0)
      {
        fprintf(fp2, "%s:%s\n", user.name, user.password);
        send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
      }
      else
        send(serverSocket, "RegError", strlen("RegError"), 0);
      fclose(fp2);
    }
    else if (same("exit", buffer) || same("3", buffer))
    {
      close(serverSocket), total--;
      break;
    }
  }
```
## Output soal 2a

Jika file server dan client di jalankan :

![output 2a](/img/run.jpeg)

Jika command register di jalankan :

![output 2a.1](/img/register.jpeg)

Tampilan dari file users.txt :

![output 2a.2](/img/users.jpeg)

## 2 b. Database Problem

Server berhasil dijalankan, akan dieksekusi fungsi fopen mode "a" yang akan membuat file problems.tsv jika sebelumnya file tersebut belum ada.

```
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
```
User dapat menambahkan problem pada file ini dengan command add yang akan dijelaskan pada bagian C. File ini berisi judul problem dan username pembuat problem yang dipisahkan oleh \t sesuai perintah soal.

```
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
```


## 2C. Add Problem
Client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan add problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Kemudian client memasukkan command add, akan muncul pesan untuk memasukkan judul problem yang akan diambil menggunakan fungsi scanf dan dikirimkan kepada server. Akan muncul juga pesan untuk memasukkan filepath description.txt yang berisi deskripsi problem, filepath input.txt yang berisi input testcase untuk menyelesaikan problem, dan output.txt yang akan digunakan untuk melakukan pengecekan pada submission client. 
```
          else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }

```
Server kemudian membuat direktori yang mengambil nama dari input judul problem yang dimasukkan oleh client dan menyimpan problem di dalamnya dengan strcpy dan strcat.

```
          else if (same("add", buffer))
          {
            problem request;
            char basePath[1100], clientPath[1100], descPath[1100], inputPath[1100];
            char descServer[1100], inputServer[1100], outputServer[1100];
            readerConn = read(serverSocket, request.problem_title, 1100);
            printf("Judul problem: %s\n", request.problem_title);
            readerConn = read(serverSocket, request.pathDesc, 1100);
            printf("Filepath description.txt: %s\n", request.pathDesc);
            readerConn = read(serverSocket, request.pathInput, 1100);
            printf("Filepath input.txt: %s\n", request.pathInput);
            readerConn = read(serverSocket, request.pathOutput, 1100);
            printf("Filepath output.txt: %s\n", request.pathOutput);
            strcpy(basePath, "problems/");
            strcat(basePath, request.problem_title);
            mkdir(basePath, 0777);
            strcat(basePath, "/");
            strcpy(descServer, basePath);
            strcat(descServer, "description.txt");
            strcpy(inputServer, basePath);
            strcat(inputServer, "input.txt");
            strcpy(outputServer, basePath);
            strcat(outputServer, "output.txt");
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
            printf("%s is adding new problem called %s to the server.\n", user.name, request.problem_title);
            continue;
          }

```
## Output soal 2c

Jika command add dijalankan  :

![output 2c](/img/add.jpeg)


## 2 d. See Problem

Client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan see problem.

```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
User memasukkan command see, akan ditampilkan. Client akan mengirimkan perintah "see" kepada server. Perintah ini akan membuat server mengembalikan daftar judul dan pembuat problem.

```
   else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;

```
Jika server mendapatkan command "see", server akan membuka file problems.tsv dengan fungsi fopen mode "r" untuk membaca problem dan mengirimkan line demi line berisi judul problem dan pengarangnya kepada client sebelum menutup file tersebut dengan fclose.

```
          else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }
```

## Output soal 2 d

Jika command see di jalankan : 

![output 2d](/img/see.jpeg)

## 2 e. Download Problem

Lali, client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan download problem.

```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika client memasukkan command download [namafile], client akan mengirimkan command tersebut menggunakan fungsi send kepada server. 

```
          else if (same(cmd, "download"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            scanf("%s", cmd);
            send(sock, cmd, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numLine = atoi(buffer);
            char inputTarget[1100];
            readerConn = read(sock, inputTarget, 1100);
            for (int i = 0; i < numLine; i++)
            {
              FILE *fpClient = fopen(inputTarget, "r");
              fpClient = fopen(inputTarget, "a+");
              char fileContent[1100];
              readerConn = read(sock, fileContent, 1100);
              fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            char fileReady[] = "File is ready to be downloaded.\n", descTarget[1100];
            if (same(buffer, fileReady))
            {
              memset(buffer, 0, sizeof(buffer));
              readerConn = read(sock, buffer, 1100);
              strcpy(descTarget, buffer);
              printf("Downloading description & input from problem %s..\n", cmd);
              int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
              if (!des_fd)
                perror("can't open file"), exit(EXIT_FAILURE);
              int file_read_len;
              char buff[1100];
              while (1)
              {
                memset(buff, 0, 1100);
                file_read_len = read(sock, buff, 1100);
                write(des_fd, buff, file_read_len);
                break;
              }
              printf("Successfully downloading the files for problem %s to the desired client directory!\n", cmd);
            }
            memset(buffer, 0, sizeof(buffer));
            continue;

```
Server

```
          else if (same("download", buffer))
          {
            readerConn = read(serverSocket, buffer, 1100);
            bool foundDesc = false, foundInput = false;
            char pathDownload[1100] = "problems/", descPath[1100], descTarget[1100];
            strcat(pathDownload, buffer);
            strcpy(descPath, pathDownload);
            strcat(descPath, "/description.txt");
            char inputPath[1100], inputTarget[1100], outputPath[1100], outputTarget[1100];
            strcpy(inputPath, pathDownload);
            strcat(inputPath, "/input.txt");
            strcpy(outputPath, pathDownload);
            strcat(outputPath, "/output.txt");
            char foundSuccess[] = "File is ready to be downloaded.\n", notFound[] = "No such file found.\n";
            fp = fopen(descPath, "r");
            FILE *source, *target;
            source = fopen(descPath, "r");
            char *line = NULL, *line2 = NULL, *line3 = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int num = 0, fd, readLength;
            while ((fileReader = getline(&line, &len, source) != -1))
            {
              num++;
              if (num > 3)
              {
                char pathContent[105];
                int i = 0, j = 0;
                while (line[i] != ':')
                  i++;
                line[i] = '\0';
                i += 2;
                while (line[i] != '\n')
                  pathContent[j++] = line[i++];
                pathContent[j] = '\0';
                if (num == 4)
                  strcpy(descTarget, pathContent);
                if (num == 5)
                  strcpy(inputTarget, pathContent);
                if (num == 6)
                  strcpy(outputTarget, pathContent);
              }
            }
            int numLine = 0;
            FILE *fpSource = fopen(inputPath, "r");
            ssize_t len2 = 0;
            ssize_t fileReader2;
            bool checkSubmission = true;
            while ((fileReader2 = getline(&line2, &len2, fpSource) != -1))
              numLine++;
            fclose(fpSource);
            fpSource = fopen(inputPath, "r");
            char buf[1100];
            sprintf(buf, "%d", numLine);
            send(serverSocket, buf, 1100, 0);
            send(serverSocket, inputTarget, 1100, 0);
            ssize_t len3 = 0;
            ssize_t fileReader3;
            while ((fileReader3 = getline(&line3, &len3, fpSource) != -1))
            {
              char outputSource[105];
              int i = 0;
              while (line3[i] != '\n')
                outputSource[i] = line3[i], i++;
              outputSource[i] = '\0';
              send(serverSocket, outputSource, 1100, 0);
            }
            fclose(fpSource);
            if (fp)
              foundDesc = true;
            if (!foundDesc)
              send(serverSocket, notFound, 1100, 0);
            else
            {
              send(serverSocket, foundSuccess, 1100, 0);
              send(serverSocket, descTarget, 1100, 0);
              fd = open(descPath, O_RDONLY);
              if (!fd)
                perror("Fail to open"), exit(EXIT_FAILURE);
              while (1)
              {
                memset(descPath, 0x00, 1100);
                readLength = read(fd, descPath, 1100);
                if (readLength == 0)
                  break;
                else
                  send(serverSocket, descPath, readLength, 0);
              }
              close(fd);
            }
            fclose(fp);
          }
```
## 2 f. Submit Problem
Client memasukkan command submit [judul-problem] [path-file-output], client akan mengirim command tersebut kepada server kemudian memasukkan [judul-problem] dan [path-file-output] untuk dikirimkan juga kepada server.

 Client kemudian akan mencoba untuk membuka file dan membacanya. Jika file tidak tersedia, akan dikembalikan pesan "Output file is not exist!" dan break akan terjadi. Jika file tersedia, program akan masuk ke dalam loop untuk mengambil line demi line dari output submission menggunakan fungsi getline untuk dikirimkan kepada server. Setelah tidak ada line untuk diambil lagi, program akan membaca perintah yang dikembalikan oleh server dan menampilkan hasil dari submission.

```
          else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }

```
Kemudian, server akan membaca informasi [judul-problem] [path-file-output] dari soal dan membuka file yang berada di destinasi path yang diberikan. Server kemudian mengambil line demi line menggunakan loop untuk membandingkan antara isi dari output yang disubmit oleh client dan output yang tersedia di database. 

```
          else if (same("submit", buffer))
          {
            char problemName[1100], pathSource[1100] = "problems/";
            readerConn = read(serverSocket, problemName, 1100);
            strcat(pathSource, problemName);
            strcat(pathSource, "/output.txt");
            FILE *fpSource = fopen(pathSource, "r");
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while (fileReader = getline(&line, &len, fpSource) != -1)
            {
              char outputSource[105], outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSource[j++] = line[i++];
              outputSource[j] = '\0';
              readerConn = read(serverSocket, outputSubmission, 105);
              int source = atoi(outputSource), submit = atoi(outputSubmission);
              printf("[%s] source: %d dan submission: %d\n", submit == source ? "AC" : "WA", source, submit);
              if (submit != source)
                checkSubmission = false;
            }
            printf("%s is %s on problem %s!\n", user.name, checkSubmission ? "accepted (AC)" : "having a wrong answer (WA)", problemName);
            send(serverSocket, checkSubmission ? "AC\nAccepted in Every Testcases\n" : "WA\nWA in the First Testcase\n", 1100, 0);
          }
```

## Output soal 2 f

Jika file download dijalankan :

![output 2f](/img/download.jpeg)

## Output soal 2b

Hasil dari folder download:

![output 2f.1](/img/download2.jpeg)


## G. Multiple Connection

Terakhir, program menangani multiple connection dengan mengecek berapa banyak client yang terkoneksi. Jika terdapat tepat satu client, server akan mengirimkan pesan connected kepada client, sedangkan jika tidak maka server akan mengirimkan pesan failed. Selama terbaca bahwa total client lebih dari satu, server akan mengecek banyak client yang terhubung. 

```
  char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them ^o^";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }

```



## :large_blue_circle: **Soal 3** :large_blue_circle: 

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

## 3 list file secara rekrusif

untuk melist file secara rekrusif, digunakan function reksv dibawah ini
```
void reksv(char *disini){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(disini);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", disini, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, eks, (void *)path);
                pthread_join(thread, NULL);
            }
            reksv(path);
        }
    } closedir(dir);
}
```

## 3. mengkategorikan file dan memindahkan ke foldernya
file-file yang ada akan di cek terlebih dahulu menggunakan function cek apakah file tersebut hidden atau terlihat
```
int cek(const char *namaF){
    struct stat banding;
    if(!stat(namaF, &banding)) return 1;
    return 0;
}
```

selanjutnya ada function eks, pada function ini akan di buat folder yang sesuai dengan nama ekstensinya yang nantinya akan dipindahan file-file tersebut ke folder yang mempunyai ekstensi yang sama

akan dibuat juga folder hidden untuk menyimpan file yang tersembunyi, ada juga folder unknown yang dibuat untuk file yang eksensinya tidak ada.

untuk menentukan nama dari ekstensinya, bisa menggunakan
```
  strtok(file, ".");
```
yang mana akan dibatasi/memisahkan namafile dari .

setelah itu akan menggunakan function strLwr pada hasil ekstensi yang didapat untuk mengubah file yang ekstensinya dalam huruf kapital menjadi huruf kecil sehingga bisa digabungkan ke folder yang sama
```
  strLwr(hasil);
```
setelah itu akan dibuat foldernya sesuai dengan ekstensi yang telah didapat dan memindahakan file ke yang dituju menggunakan fungsi C rename()

```
void *eks(void *namaF){
    char visible[200], dirname[200], cwd[PATH_MAX], hd_f[200], file[200], hd[200];
    strcpy(visible, namaF);
    strcpy(hd_f, namaF);
    char *ketemu = strrchr(hd_f, '/');
    strcpy(hd, ketemu);
    if(hd[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(namaF, ".") != NULL){
        strcpy(file, namaF);
        strtok(file, ".");
        
        char hasil[200], simpan[200];
        strcpy(hasil, "");
        strcpy(simpan, "");

        int a = strlen(visible) - 1;
       
        int ok = 0;
       
        while(a >= 0){

            if(visible[a] == '.'){
       
                if(!ok){
                    ok = 1;
                 strLwr(hasil);
                    strcpy(simpan, hasil);
                }else{
                
                    strcpy(simpan, hasil);
                    break;
                }
            }else if(visible[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", hasil, visible[a]);
            strcpy(hasil, buffer1);
            a--;
        }

        int lastt = strlen(simpan) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", simpan[lastt]);
            strcat(answer_path, buffer1);
        }
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
      
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }

    int ada = cek(visible);

    if(!strcmp(dirname, "hartakarun/_")) 
    strcpy(dirname, "hartakarun/__");
    if(ada){
    
        mkdir(dirname, 0777);

    }
    else puts("error");
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *nama = strrchr(namaF, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);

        rename(namaF, namafile);
    }
}
```

## 3. client
pada client 
digunakan pid_t untuk menzip folder harta karun dan disimpan ke directory client yang nantinya akan dikirim ke folder server

  ```
   pid_t zip;
    zip = fork();
    if(zip < 0){
        exit(EXIT_FAILURE);
    }
    if(!zip) execl("/usr/bin/zip", "zip", "-r", "hartakarun", "../hartakarun", "-q", NULL);
```

  ## 3. server  
  pada server
  pada soal diminta jika pada client menginputkan pesan"send hartakarun.zip" maka file zip itu akan dikirimkan ke folder server
  disini kamu menggunakan strcmp untuk membandingkan apakah yang telah diinputkan di client (disimpan di buffer) berisikan send hartakarun.zip, kalau sama makan akan return 0.
  setelahitu akan dilanjutkan ke proses pemindahan dari dari folder client ke server
  ```
  int hasil = strcmp(buffer,"send hartakarun.zip");
    if(hasil==0){
        char cwd[PATH_MAX];
        getcwd(cwd, sizeof(cwd));
        int apaini = 0;
        while(cwd[apaini] != '\0') apaini++;
        char ini_path[200];
        strncpy (ini_path, cwd, apaini - 7);
        char goalpath[200], startpath[200];
        strcpy(startpath, ini_path);
        strcpy(goalpath, ini_path);
        strcat(startpath, "/Client/hartakarun.zip");
        strcat(goalpath, "/Server/hartakarun.zip");
        rename(startpath, goalpath);
    }
  ```


## Output soal 3
folder hartakarun
![output 3.2](/img/3.2.png)

folder ekstensi yang berisi file yang sesuai
![output 3.1](/img/3.1.png)

salah contoh folder yang berisikan file berekstensi JPG dan jpg
![output 3.5](/img/3.5.png)

hasil zip dari client
![output 3.3](/img/3.3.png)

hasil pengiriman file ke server
![output 3.3](/img/3.3.png)


