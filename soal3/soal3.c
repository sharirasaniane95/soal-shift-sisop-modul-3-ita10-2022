#include<ctype.h>
#include<dirent.h>
#include<string.h>
#include<unistd.h>
#include<sys/stat.h>
#include<pthread.h>
#include<stdio.h>
#include<sys/types.h>

int cek(const char *namaF){
    struct stat banding;
    if(!stat(namaF, &banding)) return 1;
    return 0;
}

void strLwr(char *s) {
	int i=0;
	while(s[i]!='\0') {
	if(s[i]>='A' && s[i]<='Z') {
		s[i]=s[i]+32;
	}
	i++;
	}
}


void *eks(void *namaF){
    char visible[200], dirname[200], cwd[PATH_MAX], hd_f[200], file[200], hd[200];
    strcpy(visible, namaF);
    strcpy(hd_f, namaF);
    char *ketemu = strrchr(hd_f, '/');
    strcpy(hd, ketemu);
    if(hd[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(namaF, ".") != NULL){
        strcpy(file, namaF);
        strtok(file, ".");
        
        char hasil[200], simpan[200];
        strcpy(hasil, "");
        strcpy(simpan, "");

        int a = strlen(visible) - 1;
       
        int ok = 0;
       
        while(a >= 0){

            if(visible[a] == '.'){
       
                if(!ok){
                    ok = 1;
                 strLwr(hasil);
                    strcpy(simpan, hasil);
                }else{
                
                    strcpy(simpan, hasil);
                    break;
                }
            }else if(visible[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", hasil, visible[a]);
            strcpy(hasil, buffer1);
            a--;
        }

        int lastt = strlen(simpan) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", simpan[lastt]);
            strcat(answer_path, buffer1);
        }
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
      
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }

    int ada = cek(visible);

    if(!strcmp(dirname, "hartakarun/_")) 
    strcpy(dirname, "hartakarun/__");
    if(ada){
    
        mkdir(dirname, 0777);

    }
    else puts("error");
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *nama = strrchr(namaF, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);

        rename(namaF, namafile);
    }
}

void reksv(char *disini){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(disini);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", disini, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, eks, (void *)path);
                pthread_join(thread, NULL);
            }
            reksv(path);
        }
    } closedir(dir);
}

int main(int argc, char *argv[]){
    char cwd[PATH_MAX];
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        strcat(cwd, "/hartakarun");
        reksv(cwd);
    }
}


