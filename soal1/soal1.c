#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include"base64.h"

#define NUM_DIR 2
#define NUM_GLOBAL 2
#define SIZE 100

pid_t child_id;

pthread_t tid[NUM_DIR];
pthread_t tidGlobal[NUM_GLOBAL]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread

char __create[] = "/bin/mkdir";
char __createT[] = "/usr/bin/touch";
char __download[] = "/bin/wget";
char __unzip[] = "/usr/bin/unzip";
char __decode[] = "/bin/base64";
char __move[] = "/usr/bin/mv";
char __zip[] = "/usr/bin/zip";
char __delete[] = "/usr/bin/rm";

char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

void process(char *str, char *argv[])
{
    pid_t child_id;
    int status;

    child_id = fork();

    if(child_id == 0){
		execv(str, argv);
	}
    else
    {
        while (wait(&status) > 0)
        ;
    }
}

void checkError(int err, int i)
{
    if(err!=0) //cek error
    {
        printf("\n can't create thread : [%s]", strerror(err));
    }
    else
    {
        printf("\n create thread [%d] success\n", i + 1);
    }
}

void* createFolder(void *arg)
{
    char *directory[NUM_DIR] = {
        "/home/firdho/modul3/quote/",
        "/home/firdho/modul3/music/"
    };

    char *directoryT[NUM_DIR] = {
        "/home/firdho/modul3/quote/quote.txt",
        "/home/firdho/modul3/music/music.txt"
    };

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_DIR; i++)
    {
        char dir[100], tmp[100];
        strcpy(dir, directory[i]);
        strcpy(tmp, directoryT[i]);

        if(pthread_equal(id, tid[i])) //thread untuk clear layar
        {
            char *argv1[] = {"mkdir", dir, "-p", NULL};
            char *argv2[] = {"touch", tmp, NULL};
            process(__create, argv1);
            process(__createT, argv2);
        }
    }
	return NULL;
}


void* downloadZIP(void *arg)
{
    char *directoryDownload[NUM_GLOBAL] = {
        //quote
        //"https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
        "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt",
        //musics
        //"https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
        "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1"
    };

    char *changeName[NUM_GLOBAL] = {
        "quote.zip",
        "music.zip"
    };

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++)
    {
        char dir[100];
        char change[100];
        strcpy(dir, directoryDownload[i]);
        strcpy(change, changeName[i]);

        /*
        if(pthread_equal(id, tid[i])) //thread untuk clear layar
        {
            char *argv1[] = {"wget", "--no-check-certificate", dir, "-q", NULL};
            process(__download, argv1);
        }
        */
        char *argv1[] = {"curl", "-o", change, dir, NULL};
        //char *argv1[] = {"wget", "--no-check-certificate", dir, "-q", NULL};
        process(__download, argv1);
    }
	return NULL;
}


void* unZIP(void *arg)
{
    char *directoryUNZIP[NUM_GLOBAL] = {
        "/home/firdho/modul3/quote",
        "/home/firdho/modul3/music"
    };

    char *folder[NUM_GLOBAL] = {
        "/home/firdho/Downloads/quote.zip",
        "/home/firdho/Downloads/music.zip"
    };

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++)
    {
        char dir[100];
        char fold[100];
        strcpy(dir, directoryUNZIP[i]);
        strcpy(fold, folder[i]);

        
        if(pthread_equal(id, tidGlobal[i])) //thread untuk clear layar
        {
            //
            char *argv1[] = {"unzip", "-q", fold, "-d", dir, NULL};
            process(__unzip, argv1);
        }
        
    }
	return NULL;
}

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';    /* string padding character */
    return plain;
}

void* decodeIsi(void *arg)
{
    char *directoryDecoded[NUM_GLOBAL] = {
        "/home/firdho/modul3/quote/",
        "/home/firdho/modul3/music/"
    };    

    char *destination[NUM_GLOBAL] = {
        "/home/firdho/modul3/quote/quote.txt",
        "/home/firdho/modul3/music/music.txt"
    }; 

    char *adder[NUM_GLOBAL] = {
        "q",
        "m"
    };

    pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++)
    {
        char dir[100];
        char add[100];
        char temp[100];
        char str[100], value[100];
        
        if(pthread_equal(id, tidGlobal[i])){
            for(int j = 1; j<10; j++){
                strcpy(dir, directoryDecoded[i]);
                strcpy(add, adder[i]);
                strcat(dir, add);
                sprintf(temp, "%d.txt", j);
                strcat(dir, temp);

                printf("hasil dir: %s \n", dir);

                
                FILE *file = fopen(dir, "r"), *dest;
                if(file == NULL){
                    fclose(file);
                    printf("Error! opening file");
                }
                else{
                    printf("Yeayy ke buka \n");
                }

                
                while (fgets(str, 100, file) != NULL) {
                    printf("%s \n", str);
                    
                    unsigned char *value = base64_decode(str);
                    printf("%s \n", value);

                    dest = fopen(destination[i], "a");
                    fprintf(dest, "%s\n", value);
                }
                fclose(dest);
                fclose(file);
                
                memset(dir, 0, sizeof(dir));
                memset(value, 0, sizeof(value));
            }
        }
    }
}

void* moveFile(void *arg)
{
    char *directoryMove[NUM_GLOBAL] = {
        "/home/firdho/modul3/quote/quote.txt",
        "/home/firdho/modul3/music/music.txt"
    };

    char folder[] = "/home/firdho/modul3/hasil";

    FILE *file;
    if(file = fopen(folder, "r")){
        fclose(file);
        printf("Folder sudah ada \n");
    }
    else{
        printf("Folder belum ada \n");

        char *argv1[] = {"mkdir", folder, "-p", NULL};
        process(__create, argv1);
    }

    sleep(2);

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++)
    {
        char dir[100];
        strcpy(dir, directoryMove[i]);
        printf("%s \n", dir);
        
        if(pthread_equal(id, tidGlobal[i])) //thread untuk clear layar
        {
            //
            char *argv1[] = {"mv", dir, folder, NULL};
            process(__move, argv1);
        }

        memset(dir, 0, sizeof(dir));
    }
}

void* ziip(void *arg)
{
    char *argv1[] = {"zip", "--encrypt", "hasil.zip", "music.txt", "quote.txt", NULL};
    process(__zip, argv1);
    sleep(2);
    char *argv2[] = {"mv", "hasil.zip", "/home/firdho/modul3", NULL};
    process(__move, argv2);
}

void* gass(void *arg)
{
    char *argv1[] = {"unzip", "-q", "hasil.zip", NULL};
    process(__unzip, argv1);

    //cek ada atau tidak file no.txt
    char folder[] = "no.txt";
    FILE *file, *dest;
    if(file = fopen(folder, "r")){
        fclose(file);
        printf("File sudah ada \n");
    }
    else{
        printf("File belum ada \n");

        char *argv1[] = {"touch", folder, NULL};
        process(__createT, argv1);
    }

    sleep(2);

    //buka dan tulis
    dest = fopen(folder, "a");
    fprintf(dest, "No");
    fclose(dest);
    
    sleep(2);
    char *argv3[] = {"zip", "--encrypt", "hasil.zip", "music.txt", "quote.txt", "no.txt",NULL};
    process(__zip, argv3);

    char *argv4[] = {"rm", "-r", "hasil", "music.txt", "quote.txt", "no.txt",NULL};
    process(__delete, argv4);

}


int main(void)
{
	int err;
    /*
    |   CREATE FOLDER
    */
	for(int i = 0; i<NUM_DIR; i++) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]), NULL, &createFolder, NULL); //membuat thread
		checkError(err, i);
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
    

    /*
    |   DOWNLOAD ZIP => ERRORR
    */
	// for(int i = 0; i<NUM_GLOBAL; i++) // loop sejumlah thread
	// {
	// 	err=pthread_create(&(tid[i]), NULL, &downloadZIP, NULL); //membuat thread
	// 	checkError(err, i);
	// }
	// pthread_join(tid[0],NULL);
	// pthread_join(tid[1],NULL);


    /*
    |   UNZIP
    */
	for(int i = 0; i<NUM_GLOBAL; i++) // loop sejumlah thread
	{
		err=pthread_create(&(tidGlobal[i]), NULL, &unZIP, NULL); //membuat thread
		checkError(err, i);
	}
	pthread_join(tidGlobal[0],NULL);
	pthread_join(tidGlobal[1],NULL);


    /*
    |   DECODE 
    */
	for(int i = 0; i<NUM_GLOBAL-1; i++) // loop sejumlah thread
	{
		err=pthread_create(&(tidGlobal[i]), NULL, &decodeIsi, NULL); //membuat thread
		checkError(err, i);
	}
	pthread_join(tidGlobal[0],NULL);
	pthread_join(tidGlobal[1],NULL);


    /*
    |   MOVE
    */
	for(int i = 0; i<NUM_GLOBAL; i++) // loop sejumlah thread
	{
		err=pthread_create(&(tidGlobal[i]), NULL, &moveFile, NULL); //membuat thread
		checkError(err, i);
	}
	pthread_join(tidGlobal[0],NULL);
	pthread_join(tidGlobal[1],NULL);
    

    /*
    |   ZIP
    */
    chdir("/home/firdho/modul3/hasil");
	for(int i = 0; i<NUM_GLOBAL-1; i++) // loop sejumlah thread
	{
    err=pthread_create(&(tidGlobal[i]), NULL, &ziip, NULL); //membuat thread
		checkError(err, i);
	}
	pthread_join(tidGlobal[0],NULL);
	pthread_join(tidGlobal[1],NULL);


    /*
    |   GASSSS
    */
    chdir("/home/firdho/modul3");
	for(int i = 0; i<NUM_GLOBAL-1; i++) // loop sejumlah thread
	{
		err=pthread_create(&(tidGlobal[i]), NULL, &gass, NULL); //membuat thread
		checkError(err, i);
	}
	pthread_join(tidGlobal[0],NULL);
	pthread_join(tidGlobal[1],NULL);

	return 0;
}
